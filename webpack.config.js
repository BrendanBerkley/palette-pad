var autoprefixer = require('autoprefixer');
var sassLintPlugin = require('sasslint-webpack-plugin');
var webpack = require('webpack');

var PROD = JSON.parse(process.env.PROD_ENV || '0');

module.exports = {
	entry: './_assets/javascript/index.js',
	output: {
		path: './dist',
		publicPath: '/dist',
		filename: 'bundle.js',
	},
	devServer: {
		host: '0.0.0.0',
		inline: true,
		port: 1315
	},
	module: {
		loaders: [
			// the loaders will be applied from right to left
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: {
					presets: ['es2015', 'react']
				}
			},
			{ 
				test: /\.scss$/, 
				loaders: PROD ? [
					"style",
					"css",
					"postcss-loader",
					"sass"
				] : [
					"style",
					"css?sourceMap",
					"postcss-loader?sourceMap",
					"sass?sourceMap"
				]
			},
			{
				test: /\.png$/,
				loader: "file"
			},
			{
				test: /\.svg$/,
				loader: 'svg-sprite?' + JSON.stringify({
					name: 'icon-[name]'
				})
			}
		]
	},
	plugins: [
		new sassLintPlugin({
			context: './_assets/sass'
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(JSON.parse(('"' + process.env.NODE_ENV + '"') || '0'))
		})
	],
	postcss: [ autoprefixer({ browsers: ['last 2 versions', 'IE 9'] }) ]
}