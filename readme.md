# Palette Pad

## Install

- `npm install`
- `ln -s ../../pre-commit.sh .git/hooks/pre-commit`

## Run

- `npm run dev-server` to serve the site on http://localhost:1315
- `npm run production-server` to serve the site on http://localhost:1315; this uglifies bundle.js
- `npm run production-bundle` to uglify bundle.js to commit to repo

The first one should be all you need locally. The pre-commit hook runs `production-bundle` and adds the bundle to the repo automatically.


## Ideas/To Do

- color picker integration
- 'save' latest with localStorage
- shortcut clicks to darken, lighten, invert, etc.
- different palette layouts? (vertical, squares)
- resize?
- generate static image?
- named color slugified
- some kind of mode that lets you preview the palette. colored text on colored backgrounds, buttons, that kind of thing
- swipe to delete palette
- better responsive design - looks good on my phone on portrait but what about other devices?
- refine delete palette. maybe hide/disable icon if only one palette?
- lock down hex and rgb - lock down right-click pasting
- refactor Palette.js a bit
- beef up web app chops (ios icons not working?, load screen, android?)
- learn a testing framework in here?
- typescript?