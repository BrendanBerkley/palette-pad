import { connect } from 'react-redux';
import { deletePalette, editPalette, movePalette } from '../actions';
import PaletteGroup from '../components/PaletteGroup';

// const getVisibleTodos = (todos, filter) => {
// 	switch(filter) {
// 		case 'SHOW_ALL':
// 			return todos
// 		case 'SHOW_COMPLETED':
// 			return todos.filter(t => t.completed)
// 		case 'SHOW_ACTIVE':
// 			return todos.filter(t => !t.completed)
// 	}
// };

const mapStateToProps = (state) => {
	return {
		colors: state.colors
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onDelete: (index) => {
			dispatch(deletePalette(index))
		},
		onUpdate: (spotInArray, updatedRgb) => {
			dispatch(editPalette(spotInArray, updatedRgb))
		},
		onReorder: (oldIndex, newIndex) => {
			dispatch(movePalette(oldIndex, newIndex))
		}
	}
};

const ReduxPaletteGroup = connect(
	mapStateToProps,
	mapDispatchToProps
)(PaletteGroup);

export default ReduxPaletteGroup;