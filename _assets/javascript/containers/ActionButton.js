import { connect } from 'react-redux';
import { activateSheet, addPalette, clearPalette, removePalette } from '../actions';
import Button from '../components/Button';

const mapStateToProps = (state, ownProps) => {
	return {
		colors: state.colors
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		onClick: () => {
			switch(ownProps.action) {
				case 'ADD':
					dispatch(addPalette());
					break;
				case 'CLEAR':
					dispatch(clearPalette());
					break;
				case 'REMOVE':
					dispatch(removePalette());
					break;
				case 'SHARE':
					dispatch(activateSheet(ownProps.action));
					break;
			}
		}
	}
};

const ActionButton = connect(
	// undefined,
	mapStateToProps,
	mapDispatchToProps
)(Button);

export default ActionButton;