import { connect } from 'react-redux';
import { deactivateSheet } from '../actions';
import ActionSheets from '../components/ActionSheets';

const mapStateToProps = (state) => {
	return {
		active: (state.sheet !== false) ? true : false
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		closeSheets: () => {
			dispatch(deactivateSheet());
		}
	}
};

const ReduxActionSheets = connect(
	mapStateToProps,
	mapDispatchToProps
)(ActionSheets);

export default ReduxActionSheets;