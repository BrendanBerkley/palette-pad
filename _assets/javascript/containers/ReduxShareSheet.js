import { connect } from 'react-redux';
import { deactivateSheet } from '../actions';
import ShareSheet from '../components/ShareSheet';

const mapStateToProps = (state) => {
	return {
		colors: state.colors,
		active: (state.sheet === 'SHARE') ? true : false
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		closeSheet: () => {
			dispatch(deactivateSheet());
		}
	}
};

const ReduxShareSheet = connect(
	mapStateToProps,
	mapDispatchToProps
)(ShareSheet);

export default ReduxShareSheet;