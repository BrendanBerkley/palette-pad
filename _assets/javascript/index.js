import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import PalettePad from './components/PalettePad';
import store from './store';

require('../sass/main.scss');
require('./partials/svg.js');
require('./partials/webAppIcons.js');

render(
	<Provider store={store}>
		<PalettePad />
	</Provider>,
	document.getElementById('react-hook')
);
