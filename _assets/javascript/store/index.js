import { createStore, applyMiddleware } from 'redux';
import PalettePadReducers from '../reducers';
import helpers from '../partials/helpers';

/* Store with no initial state */
// let store = createStore(PalettePadReducers);

/* Store with initial state */
const initialState = {
	colors: helpers.getInitialColor()
}
let store = createStore(PalettePadReducers, initialState);

/* Store with initial state and action logger from Egghead's Redux Cheat Sheet */
// const actionLogger = ({dispatch, getState}) => (next) => (action) => { 
// 	console.log(action); return next (action) 
// };
// const middleware = applyMiddleware(actionLogger);
// let store = createStore(PalettePadReducers, initialState, middleware);

export default store;