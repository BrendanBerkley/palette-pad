const webAppIcons = [ 
	require('../../images/touch-icon-iphone.png'),
	require('../../images/touch-icon-ipad.png'),
	require('../../images/touch-icon-iphone-retina.png'),
	require('../../images/touch-icon-ipad-retina.png')
];

webAppIcons.forEach((image) => {
	if (image.indexOf('/dist') > -1) {
		image = image.substr(5);
	}
	const link = document.createElement('link');
	link.setAttribute('rel', 'apple-touch-icon');
	link.setAttribute('href', 'dist/' + image);
	document.getElementsByTagName('head')[0].appendChild(link);
});