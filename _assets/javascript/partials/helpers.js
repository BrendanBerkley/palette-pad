var $ = require('jquery');
import colorsys from 'colorsys';

var helpers = module.exports = {};


helpers.defaultColor = {'r': 255, 'g': 255, 'b': 255};

helpers.importColorsFromUrl = function() {
	const getColors = helpers.parseUrlGets('c');
	if (!getColors) {
		return [];
	} else {
		const colors = getColors.split('-');
		const urlColors = [];
		
		colors.forEach(function(value, index) {
			const newValue = colorsys.hexToRgb(value);
			if (newValue !== null) {
				urlColors.push(newValue);
			}
		})

		return urlColors;
	}
};

helpers.generateRandomColors = function() {
	const colors = [];
	const initialPaletteCount = 4;
	const saturation = helpers.randomRange(33, 67);
	const lightness = helpers.randomRange(33, 67);
	
	for (var i = 0; i < initialPaletteCount; i++ ) {
		colors.push(helpers.randomishRgb(saturation, lightness));
	}

	return colors;
};

helpers.getInitialColor = function() {
	let colors = helpers.importColorsFromUrl();
	if (colors.length === 0) {
		colors = helpers.generateRandomColors();
	}

	return colors;
};


// Using this while dealing with PR:
// https://github.com/kudago/Caret-position/pull/1
helpers.setCaret = function(input, start, end) {
	if (end === undefined) { end = start; }

	if (input.setSelectionRange) {
		input.focus();
		window.setTimeout(function() {
			input.setSelectionRange(start, end);
		},1);
	} else {
		var range = input.createTextRange();
		range.collapse(true);
		range.moveEnd('character', start);
		range.moveStart('character', end);
		range.select();
	}
};

// Related to loading colors from a URL.
// http://stackoverflow.com/a/5448595/
helpers.parseUrlGets = function(value) {
	var result = false;
	var tempArray = [];

	var gets = window.location.search.substr(1).split('&');
	gets.forEach(function (item) {
		tempArray = item.split('=');
		if (tempArray[0] === value) {
			result = decodeURIComponent(tempArray[1]);
		}
	});
	return result;
};

// Related to restricting keyboard input on color inputs
helpers.checkForModifierKey = function(event) {
	var modifierKeyPressed = false;

	if (event.ctrlKey || event.altKey || event.shiftKey) {
		modifierKeyPressed = true;
	}

	return modifierKeyPressed;
};

// Related to restricting keyboard input on color inputs
helpers.matchNumberToArray = function(number, array) {
	// A few presets to choose from
	if (typeof array === 'string') {
		switch (array) {
			case 'numbers':
				array = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
				break;
			case 'rgb':
				array = [8, 9, 16, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53,
						54, 55, 56, 57];
				break;
			case 'hex': 
				array = [8, 9, 16, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53,
						54, 55, 56, 57, 65, 66, 67, 68, 69, 70];
				break;
			default:
				array = [];
		}
	}

	var match = false;
	for (var i = 0; i < array.length; i++) {
		if (number === array[i]) {
			match = true;
		}
		if (i === array.length - 1) {
			return match;
		}
	}
};

helpers.calculateNewValue = function(event, currentValue, max) {
	var increment;
	if (event.keyCode === 38) {
		increment = 1;
	} else if (event.keyCode === 40) {
		increment = -1;
	}	
	if (event.shiftKey) {
		increment = increment * 10;
	}
	var newValue = currentValue + increment;
	
	if (newValue > max) { 
		newValue = max; 
	} else if (newValue < 0) { 
		newValue = 0; 
	}

	return newValue;
};


helpers.randomRgb = function() {
	var rgb = {
		'r': this.randomRange(0,255),
		'g': this.randomRange(0,255),
		'b': this.randomRange(0,255)
	};

	return rgb;
};

helpers.randomishRgb = function(saturation, lightness) {
	var hsl = {
		'h': this.randomRange(0,255),
		's': saturation,
		'l': lightness
	};
	var rgb = colorsys.hslToRgb(hsl);

	return rgb;
};

// Picks a random number between a range of integers.
helpers.randomRange = function(min, max) {
	// http://stackoverflow.com/a/7228322/
	return Math.floor(Math.random() * (max-min + 1) + min);
};