import React from 'react';

const ColorInput = ({ type, fieldName, value, blur, focus, update, keyDown}) => (
	<input 
		data-type={type}
		className="palette__input-rgb"
		type="text"
		maxLength="3"
		name={fieldName}
		value={value}
		onBlur={blur}
		onFocus={focus}
		onChange={update}
		onKeyDown={keyDown}
	/>
);

export default ColorInput;