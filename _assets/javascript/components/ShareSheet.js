import React from 'react';
import colorsys from 'colorsys';

const ShareSheet = ({ colors, active, closeSheet }) => {
	let hexOutput = '';

	colors.map((color, index) => {
		hexOutput += colorsys.rgbToHex(color).substr(1);
		if (index < colors.length - 1) {
			hexOutput += '-';
		}
	});

	let baseUrl = window.location.origin + window.location.pathname;

	return (
		<div className={active ? 'action-sheets__share action-sheets__share--active' : 'action-sheets__share '}>

			<div className="action-sheets__close" onClick={closeSheet}>
				<svg className="action-sheets__close__icon"><use xlinkHref="#icon-close" /></svg>
			</div>
			
			Copy and paste this URL to share your palette:<br />
			<input 
				className="action-sheets__text-input"
				onClick={(event) => { event.target.setSelectionRange(0, event.target.value.length) }}
				readOnly
				type="text" 
				value={baseUrl + '?c=' + hexOutput} 
			/>
			
		</div>
	);
};


export default ShareSheet;
