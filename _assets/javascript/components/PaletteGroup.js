import React from 'react';
import Sortable from 'sortablejs';
import SortableMixin from 'sortablejs/react-sortable-mixin';
import Palette from './Palette';

// If I do the mixin, I don't think this can be a stateless functional 
// component anymore.
// const ButtonGroup = ({ colors, onDelete, onUpdate }) => {
const ButtonGroup = React.createClass({
	mixins: [SortableMixin],

	handleSort: function(event) {
		this.props.onReorder(event.oldIndex, event.newIndex);
	},

	sortableOptions: {
		animation: 250,
		handle: '.palette__block',
		model: 'colors',
		// onEnd: function(event) {
		// onUpdate: function(event) {
		// 	this.props.onReorder(event.oldIndex, event.newIndex);
		// 	event.preventDefault();
		// },
	},
		
	render: function() {
		return (
			// <ul className="palettes" ref="list">
			<ul className="palettes">
				{this.props.colors.map((color, index) =>
					<Palette
						key={index}
						spotInArray={index}
						initialRgb={color}
						delete={() => this.props.onDelete(index)}
						update={this.props.onUpdate}
					/>
				)}
			</ul>
		)
	}
});

export default ButtonGroup;

