import colorsys from 'colorsys';
import getCaretPosition from 'caret-position2/get';
import ntc from 'ntc';
import React from 'react';
import { connect } from 'react-redux';

import helpers from '../partials/helpers.js';

import ColorInput from './ColorInput';
import ColorSlider from './ColorSlider';

const Palette = React.createClass({
	// http://stackoverflow.com/a/11868159/
	// https://www.w3.org/TR/AERT#color-contrast
	// http://jsfiddle.net/PXJ2C/
	brightnessCheck: function(rgb) {
		var brightness = ((rgb.r * 299) + (rgb.g * 587) + (rgb.b * 114)) / 1000;
		var isColorBright = false;
		if (brightness > 125) {
			isColorBright = true;
		}
		return isColorBright;
	},

	getInitialState: function() {
		return {
			rgb: this.props.initialRgb,
			hex: colorsys.rgbToHex(this.props.initialRgb).substr(1),
			hsl: colorsys.rgbToHsl(this.props.initialRgb)[0],
			hsv: colorsys.rgbToHsv(this.props.initialRgb)[0],
			focused: false
		};
	},

	componentWillReceiveProps: function(nextProps) {
		var rMatch = (nextProps.initialRgb.r === this.state.rgb.r);
		var gMatch = (nextProps.initialRgb.g === this.state.rgb.g);
		var bMatch = (nextProps.initialRgb.b === this.state.rgb.b);
		var colorMatch = (rMatch && gMatch && bMatch);

		if (!colorMatch) {
			this.setState({ 
				rgb: nextProps.initialRgb,
				hex: colorsys.rgbToHex(nextProps.initialRgb).substr(1),
				hsl: colorsys.rgbToHsl(nextProps.initialRgb)[0],
				hsv: colorsys.rgbToHsv(nextProps.initialRgb)[0],
				focused: false
			});
		}
	},

	update: function(event, passedNewValue) {
		// console.log('store in palette.js');
		// console.log(this);
		// console.log(this.context);
		// console.log(this.context.store.getState());

		var newValue;
		if (typeof passedNewValue !== 'undefined') {
			newValue = passedNewValue;
		} else {
			newValue = event.target.value;
		}

		var fieldName = event.target.name;
		var fieldType = event.target.getAttribute('data-type');

		var updatedRgb = this.state.rgb;
		var updatedHex = this.state.hex;
		var updatedHsl = this.state.hsl;
		var updatedHsv = this.state.hsv;

		switch (fieldType) {
			case 'rgb':
				if (!this.validateInput(newValue, 'numbers') || newValue > 255) {
					newValue = 255;
				} else if (newValue < 0) {
					newValue = 0;
				}

				updatedRgb[fieldName] = newValue;

				if (!isNaN(newValue) && newValue !== '') {
					updatedHex = colorsys.rgbToHex(updatedRgb).substr(1);
					updatedHsl = colorsys.rgbToHsl(updatedRgb)[0];
					updatedHsv = colorsys.rgbToHsv(updatedRgb)[0];
				}
				break;
			case 'hsl':
				if (fieldName === 'h') {
					if (!this.validateInput(newValue, 'numbers') || newValue > 360) {
						newValue = 360;
					}
				} else {
					if (!this.validateInput(newValue, 'numbers') || newValue > 99) {
						newValue = 99;
					} else if (newValue < 0) {
						newValue = 0;
					}
				}
				updatedHsl[fieldName] = newValue;
				if (!isNaN(newValue) && newValue !== '') {
					updatedRgb = colorsys.hslToRgb(updatedHsl);
					updatedHex = colorsys.hslToHex(updatedHsl).substr(1);
					updatedHsv = colorsys.rgbToHsv(updatedRgb)[0];
				} 
				break;
			case 'hsv':
				if (fieldName === 'h') {
					if (!this.validateInput(newValue, 'numbers') || newValue > 360) {
						newValue = 360;
					}
				} else {
					if (!this.validateInput(newValue, 'numbers') || newValue > 99) {
						newValue = 99;
					} else if (newValue < 0) {
						newValue = 0;
					}
				}
				updatedHsv[fieldName] = newValue;
				if (!isNaN(newValue) && newValue !== '') {
					updatedRgb = colorsys.hsvToRgb(updatedHsv);
					updatedHex = colorsys.hsvToHex(updatedHsv).substr(1);
					updatedHsl = colorsys.rgbToHsl(updatedRgb)[0];
				} 
				break;
			default:
				if (!this.validateInput(newValue, 'hex')) { 
					newValue = 'FFFFFF'; 
				}
				updatedHex = newValue;
				if (newValue.length === 6) {
					updatedRgb = colorsys.hexToRgb(newValue);
					updatedHsl = colorsys.hexToHsl(newValue);
					updatedHsv = colorsys.hexToHsv(newValue);
				}
		}

		this.props.update(this.props.spotInArray, updatedRgb);

		this.setState({ 
			rgb: updatedRgb,
			hex: updatedHex,
			hsl: updatedHsl,
			hsv: updatedHsv,
		})
	},

	conditionalBlur: function(event) {
		var target = event.currentTarget;
		if (!target.contains(document.activeElement)) {
			this.updateBlur(event);
		}
	},

	updateBlur: function(event) {
		var newValue = event.target.value;
		var fieldName = event.target.name;
		var fieldType = event.target.getAttribute('data-type');

		if (newValue === '') {
			switch (fieldType) {
				case 'rgb':
					newValue  = 255;
					break;
				case 'hsl':
				case 'hsv':
					if (fieldName === 'h') {
						newValue = 360;
					} else {
						newValue = 99;
					}
					break;
				default:
					newValue = 'FFFFFF';
			}

			this.update(event, newValue);
		}
		this.setState({ 
			focused: false
		});
	},

	updateFocus: function(event) {
		this.setState({ 
			focused: true
		});
	},

	updateHexWithKeyboard: function(event) {
		var currentValue = event.target.value;
		var updatedHex = this.state.hex;

		// If the keyCode matches our list, OR if ctrl, alt, or shift is being 
		// pressed, AND if the user is *not* pressing shift and a number together
		if ((helpers.matchNumberToArray(event.keyCode, 'hex') || 
				helpers.checkForModifierKey(event)) &&
			!(event.shiftKey && helpers.matchNumberToArray(event.keyCode, 'numbers'))) {
			// If up or down arrows
			if (event.keyCode === 38 || event.keyCode === 40) {
				// Change the value via keyboarding
				// Get caret position
				var caret = getCaretPosition(event.target).caret;
				// Split into RGB values
				// http://stackoverflow.com/q/2297347/
				var value = currentValue.match(/.{1,2}/g);
				// Pick the one we want to work with based on caret position
				var valueToChange = 0;
				if (caret > 2 && caret < 5) {
					valueToChange = 1;
				} else if (caret > 4) {
					valueToChange = 2;
				}
				var specificValue = parseInt(value[valueToChange], 16);
				var newValue = helpers.calculateNewValue(event, specificValue, 255);
				newValue = newValue.toString(16);
				if (newValue.length === 1) { newValue = '0'+newValue; }

				value[valueToChange] = newValue;
				updatedHex = value[0] + value[1] + value[2];

				helpers.setCaret(event.target, caret);
				
				this.update(event, updatedHex);
			}
		} else {
			event.preventDefault();
		}
	},
	
	updateWithKeyboard: function(event) {
		var currentValue = event.target.value;
		var fieldName = event.target.name;
		var fieldType = event.target.getAttribute('data-type');
		var updatedRgb = this.state.rgb;

		// If the keyCode matches our list, OR if ctrl, alt, or shift is being 
		// pressed, AND if the user is *not* pressing shift and a number together
		if ((helpers.matchNumberToArray(event.keyCode, 'rgb') || 
				helpers.checkForModifierKey(event)) &&
			!(event.shiftKey && helpers.matchNumberToArray(event.keyCode, 'numbers'))) {
			// If up or down arrows
			if (event.keyCode === 38 || event.keyCode === 40) {
				// Change the value via keyboarding
				var newValue;
				if (fieldType === 'rgb') {
					newValue = helpers.calculateNewValue(event, parseInt(currentValue), 255);
				} else {
					if (fieldName === 'h') {
						newValue = helpers.calculateNewValue(event, parseInt(currentValue), 360);
					} else {
						newValue = helpers.calculateNewValue(event, parseInt(currentValue), 99);
					}
				}
				this.update(event, newValue);
			}
		} else {
			event.preventDefault();
		}
	},

	render: function() {
		return (
			<li 
				className={this.state.focused ? 'palette palette--focused' : 'palette'}
				onMouseOver={this.updateFocus}
				onMouseOut={this.conditionalBlur}
			>
				<div className={this.brightnessCheck(this.state.rgb) ? 'palette__inputs' : 'palette__inputs palette--dark'}>

					<div className="palette__inputs-group palette__inputs-group--keyboard">
						#<input 
							className="palette__input-hex" 
							type="text" 
							maxLength="6" 
							name="hex" 
							value={this.state.hex} 
							onChange={this.update} 
							onFocus={this.updateFocus}
							onBlur={this.updateBlur} 
							onKeyDown={this.updateHexWithKeyboard}
						/>
					</div>
					<div className="palette__inputs-group palette__inputs-group--keyboard">
						rgb(<ColorInput
								type="rgb"
								fieldName="r"
								value={this.state.rgb.r}
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>,<ColorInput
								type="rgb"
								fieldName="g"
								value={this.state.rgb.g}
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>,<ColorInput
								type="rgb"
								value={this.state.rgb.b}
								fieldName="b"
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>)
					</div>
					<div className="palette__inputs-group palette__inputs-group--keyboard">
						hsl(<ColorInput 
								type="hsl"
								fieldName="h"
								value={this.state.hsl.h}
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>,<ColorInput
								type="hsl"
								fieldName="s"
								value={this.state.hsl.s}
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>,<ColorInput
								type="hsl"
								fieldName="l"
								value={this.state.hsl.l}
								update={this.update}
								blur={this.updateBlur}
								focus={this.updateFocus}
								keyDown={this.updateWithKeyboard}
							/>)
					</div>
					<div className="palette__inputs-group palette__inputs-group--keyboard">
						hsv(<ColorInput
							type="hsv"
							fieldName="h"
							value={this.state.hsv.h}
							update={this.update}
							blur={this.updateBlur}
							focus={this.updateFocus}
							keyDown={this.updateWithKeyboard}
						/>,<ColorInput
							type="hsv"
							fieldName="s"
							value={this.state.hsv.s}
							update={this.update}
							blur={this.updateBlur}
							focus={this.updateFocus}
							keyDown={this.updateWithKeyboard}
						/>,<ColorInput
							type="hsv"
							fieldName="v"
							value={this.state.hsv.v}
							update={this.update}
							blur={this.updateBlur}
							focus={this.updateFocus}
							keyDown={this.updateWithKeyboard}
						/>)
					</div>

					<div className="palette__outputs-touch">
						rgb({this.state.rgb.r},{this.state.rgb.g},{this.state.rgb.b}) - #{this.state.hex}
					</div>

					<div className="palette__matched-name">
						{this.setMatchedName(this.state.hex)}
					</div> 

					<div className="palette__inputs-group palette__inputs-group--touch">
						<ColorSlider type="rgb" fieldName="r" value={this.state.rgb.r} update={this.update} />
						<ColorSlider type="rgb" fieldName="g" value={this.state.rgb.g} update={this.update} />
						<ColorSlider type="rgb" fieldName="b" value={this.state.rgb.b} update={this.update} />
					</div>

				</div>

				<div className="palette__close" onClick={this.props.delete} data-ref={this.props.spotInArray}>
					<svg className="palette__close__icon" data-ref={this.props.spotInArray}><use xlinkHref="#icon-trash" data-ref={this.props.spotInArray} /></svg>
				</div>

				<div className="palette__done" onClick={this.updateBlur}>
					<svg className="palette__done__icon"><use xlinkHref="#icon-check" /></svg>
				</div>

				<div className="palette__block" style={{
					backgroundColor: 'rgb(' + this.state.rgb.r +', '+ this.state.rgb.g + ', ' + this.state.rgb.b + ')'
				}} />
			</li>
		);
	},

	setMatchedName: function(hexValue) {
		var matchedName  = ntc.name(hexValue);
		return matchedName[1];
	},

	validateInput: function(value, array) {
		if (typeof value === 'number') {
			value = value.toString();
		}
		var inputValid = true;
		var valueAsArray = value.split('');
		var okayArray = array;

		if (typeof array === 'string') {
			if (array === 'numbers') {
				okayArray = ['0','1','2','3','4','5','6','7','8','9'];
			} else if (array === 'hex') {
				okayArray = ['0','1','2','3','4','5','6','7','8','9','a','A','b',
					'B','c','C','d','D','e','E','f','F'];
			}
		}

		valueAsArray.forEach(function(value, index) {
			if (okayArray.indexOf(value) === -1) {
				inputValid = false;
			}
		});

		return inputValid;
	}
});

export default Palette;



// let mapStateToProps = function(state) {
//   return {
//     colorz: state.get('colors'),
//     namez: state.get('name')
//   };
// }

// export const PaletteContainer = connect(mapStateToProps)(Palette);