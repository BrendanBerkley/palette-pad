import React from 'react';
import ActionButton from '../containers/ActionButton';

const ButtonGroup = () => (
	<div className="buttons">
		<ActionButton icon="plus" action="ADD" />
		<ActionButton icon="minus" action="REMOVE" />
		<ActionButton icon="trash" action="CLEAR" />
		<ActionButton icon="share" action="SHARE" />
	</div>
);

export default ButtonGroup;