import React from 'react';

const ColorSlider = ({ type, fieldName, value, update }) => (
	<input 
		className="palette__slider" 
		type="range"
		data-type={type}
		min="0" 
		max="255" 
		name={fieldName}
		value={value} 
		onChange={update} 
	/>
);

export default ColorSlider;