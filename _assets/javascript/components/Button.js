import React from 'react';

const Button = ({ onClick, icon, colors }) => (
	<button 
		className="buttons__button" 
		onClick={onClick}
	><svg className="buttons__icon"><use xlinkHref={"#icon-"+icon} /></svg></button>
);

export default Button;