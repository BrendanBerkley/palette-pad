import React from 'react';
import ButtonGroup from './ButtonGroup';
import ReduxPaletteGroup from '../containers/ReduxPaletteGroup';
import ReduxActionSheets from '../containers/ReduxActionSheets';

const PalettePad = () => (
	<div className="palette-pad">
		<div className="menu">
			<div className="logo" title="Palette Pad">P</div>
		</div>
		<ReduxPaletteGroup />
		<ButtonGroup />
		<ReduxActionSheets />
	</div>
);

export default PalettePad;
