import React from 'react';
import ReduxShareSheet from '../containers/ReduxShareSheet';

const ActionSheets = ({ active, closeSheets }) => (
	<div className={active ? 'action-sheets action-sheets--active' : 'action-sheets '}>
		<div className="action-sheets__background" onClick={closeSheets}></div>
		<ReduxShareSheet />
	</div>
);

export default ActionSheets;