export const addPalette = () => { 
	return { 
		type: 'ADD_PALETTE' 
	};
};

export const clearPalette = () => { 
	return { 
		type: 'CLEAR_PALETTE' 
	};
};

export const deletePalette = (index) => { 
	return { 
		type:  'DELETE_PALETTE',
		index
	};
};

export const editPalette = (spotInArray, updatedRgb) => {
	return {
		type: 'EDIT_PALETTE',
		index: spotInArray,
		newColor: updatedRgb
	};
};

export const movePalette = (oldIndex, newIndex) => {
	return {
		type: 'MOVE_PALETTE',
		oldIndex,
		newIndex
	};
};

export const removePalette = () => {
	return { 
		type: 'REMOVE_PALETTE' 
	};
};

export const activateSheet = (sheet) => {
	return { 
		type: 'ACTIVATE_SHEET',
		sheet
	};
};

export const deactivateSheet = () => {
	return { 
		type: 'DEACTIVATE_SHEET',
	};
};