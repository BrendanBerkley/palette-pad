const sheetVisibilityReducer = (state = false, action) => {
	switch (action.type) {
		case 'ACTIVATE_SHEET': return action.sheet;
		case 'DEACTIVATE_SHEET': return false;
		default:
			return state;
	}
};

export default sheetVisibilityReducer;