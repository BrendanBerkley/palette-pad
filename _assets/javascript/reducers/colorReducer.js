import helpers from '../partials/helpers.js';

const colorReducer = (state = [], action) => {
	switch (action.type) {
		case 'ADD_PALETTE'   : return state.slice(0).concat([helpers.defaultColor]);

		case 'CLEAR_PALETTE' : return [helpers.defaultColor];

		case 'DELETE_PALETTE':
			return state
				.slice(0, action.index)
				.concat(state.slice(parseInt(action.index) + 1));

		case 'MOVE_PALETTE':
			let updatedState = state.slice(0);
			// http://stackoverflow.com/a/7180095/945370
			updatedState.splice(action.newIndex, 0, updatedState.splice(action.oldIndex, 1)[0]);
			return updatedState;
			// 'pure' function would be something like this, but you'd need something else
			// for when palettes are dragged from right to left
			// return state
			// 	.slice(0, action.oldIndex)
			// 	.concat(state.slice(action.oldIndex + 1, action.newIndex + 1))
			// 	.concat(state[action.oldIndex])
			// 	.concat(state.slice(action.newIndex + 1));

		case 'REMOVE_PALETTE': return state.slice(0, state.length - 1);

		case 'EDIT_PALETTE':
			return state
				.slice(0, action.index)
				.concat(action.newColor)
				.concat(state.slice(action.index + 1));
				
		default:
			return state;
	}
};

export default colorReducer;