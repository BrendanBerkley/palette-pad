import { combineReducers } from 'redux';

import colorReducer from './colorReducer';
import sheetVisiblityReducer from './sheetVisibilityReducer';

const PalettePadReducers = combineReducers ({
	colors: colorReducer, 
	sheet: sheetVisiblityReducer
});

export default PalettePadReducers;